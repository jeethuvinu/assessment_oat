<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Config;




class QuestionController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getQuestions($lang)
    {
        $tr = new GoogleTranslate();
        $tr->setSource('en');
        $tr->setSource();
        $tr->setTarget($lang);

        if (config('constants.GET_QUESTION_SOURCE') == 1) {
            $jsonFilePath   = storage_path() . "/questions.json";
            $questionInfo = json_decode(file_get_contents($jsonFilePath), true);
            $data = array();
            foreach ($questionInfo as $qinfo) {
                $arr = array();
                $arr['question'] = $tr->translate($qinfo['text']);
                $arr['createdAt'] = $tr->translate($qinfo['createdAt']);
                $arr['choices'][0]['text']  = $tr->translate($qinfo['choices'][0]['text']);
                $arr['choices'][1]['text']  = $tr->translate($qinfo['choices'][1]['text']);
                $arr['choices'][2]['text']  = $tr->translate($qinfo['choices'][2]['text']);
                array_push($data, $arr);
            }
        }

        if (config('constants.GET_QUESTION_SOURCE') == 2) {
            $csvfilenPath   = storage_path() . "/questions.csv";
            $delimiter = ',';
            if (!file_exists($csvfilenPath) || !is_readable($csvfilenPath))
                return false;

            $header = null;
            $data = array();
            if (($handle = fopen($csvfilenPath, 'r')) !== false) {
                while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                    if (!$header)
                        $header = $row;
                    else {
                        $arr = array();
                        $arr['question'] = $tr->translate($row[0]);
                        $arr['createdAt'] = $tr->translate($row['1']);
                        $arr['choices'][0]['text']  = $tr->translate($row[2]);
                        $arr['choices'][1]['text']  = $tr->translate($row[3]);
                        $arr['choices'][2]['text']  = $tr->translate($row[4]);
                        array_push($data, $arr);
                    }
                }
                fclose($handle);
            }
        }
        return response()->json(['description' => "List of translated questions and associated choices", 'content' => $data, 'status_code' => 200]);
    }

    public function storeQuestions(Request $request)
    {
        $data            = $request->all();
        $data['createdAt'] =  date('Y-m-d H:i:s');

        // Store data to a json file
        $inp        = file_get_contents(storage_path() . "/questions.json");
        $tempArray  = json_decode($inp);
        array_push($tempArray, $data);
        $jsonData   = json_encode($tempArray);
        file_put_contents(storage_path() . "/questions.json", $jsonData);

        // // Store data to a csv file
        $file_open = fopen(storage_path() . "/questions.csv", 'a');
        fputcsv($file_open, array($data['text'], $data['createdAt'], $data['choices'][0]['text'], $data['choices'][1]['text'], $data['choices'][2]['text']));
        return response()->json(['description' => "Successfully Added", 'status_code' => 200]);
    }


}
